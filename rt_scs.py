import rt

import passwd
# passwd.py needs three variables:
# url = 'rt-url.com'
# username = 'login-user'
# password = 'login-password'

srt = rt.Rt(passwd.url + '/REST/1.0/', passwd.username, passwd.password)


def do_delete(args):
    """
        Delete the specified tickets; called via argparse.
        """
        t = args.ticket
        if len(t) == 1:
                t.append(t[0])
        start = min(t)
        end = max(t)
        delete(start, end, args.queue)


def do_take(args):
    """
        Take the ticket and assign it to my queue.
        """
        try:
                take(args.ticket, args.into)
        except AttributeError:
                take(args.ticket)


def do_resolve(args):
        """ Resolve the ticket. """
        try:
                srt.reply(args.ticket, args.message)
        except (AttributeError, TypeError):
                pass
        resolve(args.ticket)


def do_stall(args):
        """ Stall a ticket. """
        try:
                srt.comment(args.ticket, args.comment)
        except (AttributeError, TypeError):
                pass
        stall(args.ticket)


def do_merge(args):
        """
        Merge two or more tickets (as a range) together.
        """
        # If ticket contains only one, merge it into the --into.
        # If ticket contains two items, treat it as a range.
        if len(args.ticket) == 1:
                merge(args.ticket[0], args.into)
        else:
                merges(args.ticket[0], args.ticket[1], args.into)


def do_read(args):
        if args.ticket:
                import pprint
                pprint.pprint(srt.get_ticket(args.ticket))
                for h in srt.get_history(args.ticket):
                        print(str(ascii(h['Content']).replace('\\n', '\n')))
                        #pprint.pprint(str(ascii(h['Content']).replace('\\n', '\n')))


def do_report(args):
        try:
                if args.new:
                        print(srt.new_correspondence)
        except AttributeError:
                pass


def do_message(args):
        try:
                if not args.comment:
                        srt.reply(args.ticket, args.message, args.cc, args.bcc)
                else:
                        srt.comment(args.ticket, args.cc, args.bcc)
        except AttributeError:
                srt.comment(args.ticket, args.cc, args.bcc)


    #TODO: Safe by default (don't delete when the requestor is ESR/BTS)
"""
Delete the tickets between start and end, inclusively, within the specified
queue (defaults to the General queue).
"""
def delete(start, end, queue="General"):
    tickets = range(int(start), int(end)+1)

    for t in tickets:

        current_ticket = srt.get_ticket(t);
        if current_ticket["Status"] == "deleted":
            print("Ticket " + str(t) + " is already deleted.")
            continue
        if current_ticket["Queue"] != queue:
            print("Not deleting ticket " + str(t) + " in queue " + current_ticket["Queue"])
            continue

        print("Deleting ticket " + str(t))
        if srt.edit_ticket(t, Status="deleted"):
            print("Ticket deleted.")
        else:
            print("Failed to delete " + str(t))


def get(id):
    """
    Retrieve the ticket with the specified ID.
    """
    return srt.get_ticket(id)


def take(id, resolve_ticket=False):
    """
    Assign the ticket to me and add it to the 'desktop' queue.
    If resolve=True, resolves the ticket as well.
    """
    #if srt.edit_ticket(id, Owner="Ryan Frame", Queue="desktop"):
    #if srt.edit_ticket(id, Owner=passwd.username) and srt.edit_ticket(id, Queue="desktop"):
    if srt.edit_ticket(id, Owner=passwd.username, Queue="desktop"):
        print("Took ticket #" + str(id))
        if resolve_ticket: resolve(id)
    else:
        print("Failed to take ticket #" + str(id))


def resolve(id):
    """
    Resolve the ticket.
    """
    if srt.edit_ticket(id, Status='resolved'):
        print('Resolved ticket #' + str(id))
    else:
        print('Failed to resolve ticket #' + str(id))


def stall(id):
        """ Stall a ticket. """
        if srt.edit_ticket(id, Status='stalled'):
                print('Stalled ticket #' + str(id))
        else:
                print('Failed to mark ticket #{0} as stalled.'.format(str(id)))


def merge(ticket_id, into_id):
    """
    Merge the into_id ticket into the ticket_id ticket.

    ticket_id may be a list of tickets (e.g., merge(list(1,3), 7)).
    """

    if type(ticket_id) is list:
        for t in ticket_id:
            merge(t, into_id)
        return

    if ticket_id == into_id:
        print("Not merging ticket " + str(ticket_id) + " into itself.")

    if srt.merge_ticket(ticket_id, into_id):
        print("Merged " + str(ticket_id) + " into " + str(into_id) + ".")
    else:
        print("Failed to merge " + str(ticket_id) + " into " + str(into_id) + ".")


def merges(tickets_start_id, tickets_end_id, into_id):
    """
    Merge the tickets between start and end (inclusively) into the into_id
    ticket.
    """
    tickets = range(int(tickets_start_id), int(tickets_end_id)+1)
    for t in tickets:
        merge(t, into_id)


if __name__ == "__main__":
    import argparse

        parser = argparse.ArgumentParser(description='Manage the RT queues.')
        subparsers = parser.add_subparsers()

        delete_parser = subparsers.add_parser('delete', help='Delete one or more tickets.')
        delete_parser.add_argument('--queue', '-q', default='General')
        delete_parser.add_argument('ticket', type=int, nargs='+', default=None)
        delete_parser.set_defaults(func=do_delete)

        take_parser = subparsers.add_parser('take', help='Take a ticket.')
        take_parser.add_argument('ticket', type=int)
        take_parser.add_argument('--resolve', '-r', default=False)
        take_parser.set_defaults(func=do_take)

        resolve_parser = subparsers.add_parser('resolve', help='Resolve a ticket.')
        resolve_parser.add_argument('ticket', type=int)
        resolve_parser.add_argument('--message', '--msg', '-m')
        resolve_parser.set_defaults(func=do_resolve)

        merge_parser = subparsers.add_parser('merge', help='Merge tickets.')
        merge_parser.add_argument('ticket', type=int, nargs='+', default=None)
        merge_parser.add_argument('--into', type=int, required=True)
        merge_parser.set_defaults(func=do_merge)

        stall_parser = subparsers.add_parser('stall', help='Stall a ticket.')
        stall_parser.add_argument('ticket', type=int)
        stall_parser.add_argument('--comment', '-c', '-m')
        stall_parser.set_defaults(func=do_stall)

        read_parser = subparsers.add_parser('read')
        read_parser.add_argument('ticket', type=int, default=None)
        read_parser.set_defaults(func=do_read)

        report_parser = subparsers.add_parser('report')
        report_parser.add_argument('--new')
        report_parser.set_defaults(func=do_report)

        message_parser = subparsers.add_parser('send')
        message_parser.add_argument('--comment')
        message_parser.add_argument('--cc', default='')
        message_parser.add_argument('--bcc', default='')
        message_parser.add_argument('--ticket', '-t', type=int, default='')
        message_parser.add_argument('message', default='')
        message_parser.set_defaults(func=do_message)

        srt.login()

        args = parser.parse_args()
        args.func(args)

